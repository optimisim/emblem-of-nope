const browser = require("webextension-polyfill");

async function getFilters() {
	const result = browser.storage.local.get("filters");

	try {
		return (await result).filters || {};
	} catch (e) {
		return {};
	}
}

async function isPluginEnabled() {
	const result = browser.storage.local.get("enabled");

	try {
		return (await result).enabled || false;
	} catch (e) {
		return false;
	}
}

async function validate(details) {
	const filters = await getFilters();

	for (const guid in filters) {
		const filter = filters[guid];
		const newUrl = details.url.replace(filter.regex, filter.target);

		if (details.url !== newUrl) {
			browser.tabs.update(details.tabId, {
				url: newUrl,
				loadReplace: true,
			});
			break;
		}
	}
}

browser.webNavigation.onBeforeNavigate.addListener(async (details) => {
	if (details.frameId === 0 && (await isPluginEnabled())) {
		validate(details);
	}
});
