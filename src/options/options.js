const browser = require("webextension-polyfill");

function getDefaultFilter() {
	return {
		regex: /developer.chrome.com/i,
		target: "developer.mozilla.com",
	};
}

async function getFilters() {
	const result = browser.storage.local.get("filters");

	try {
		return (await result).filters || {};
	} catch (e) {
		return {};
	}
}

function buildFilter(filter, guid) {
	let container = document.createElement("li");
	container.id = guid;
	container.classList.add("list-group-item", "d-flex", "align-items-start");

	container.innerHTML = `
<div class="container ms-2 me-auto justify-content-between w-100 border-start border-end">
	<div class = "d-flex flex-column flex-md-row ">
		<div class="mb-3 w-100 me-sm-5">
			<label for="regex_source${guid}" class="form-label">Regex</label>
			<input type="text" class="form-control" id="regex_source${guid}" placeholder="Regex" value="${filter.regex.source}">
		</div>
		<div class="mb-3">
			<label for="regex_flags${guid}" class="form-label">Regex Flags</label>
			<input type="text" class="form-control" id="regex_flags${guid}" placeholder="Regex Flags" value="${filter.regex.flags}">
		</div>
	</div>
	<div class="mb-3">
		<label for="target${guid}" class="form-label">Redirect</label>
		<input type="text" class="form-control" id="target${guid}" placeholder="Redirect"  value="${filter.target}">
	</div>
</div>
  `;
	const inputs = container.getElementsByTagName("input");
	for (let i = 0; i < inputs.length; i++) {
		inputs[i].addEventListener("blur", () => updateFilter(guid));
	}

	const deleteButton = document.createElement("span");
	deleteButton.classList.add("btn", "btn-outline-danger", "my-auto", "ms-3");
	deleteButton.innerText = "🗑";
	deleteButton.addEventListener("click", () => deleteFilter(guid));

	container.appendChild(deleteButton);

	return container;
}

async function refreshFilters() {
	const filters = await getFilters();

	for (let guid in filters) {
		document
			.getElementById("filters")
			.appendChild(buildFilter(filters[guid], guid));
	}
}

function nextGuid(filters) {
	let guid = "";

	do {
		const S4 = function () {
			return (((1 + Math.random()) * 0x10000) | 0)
				.toString(16)
				.substring(1);
		};
		guid =
			S4() +
			S4() +
			"-" +
			S4() +
			"-" +
			S4() +
			"-" +
			S4() +
			"-" +
			S4() +
			S4() +
			S4();
	} while (filters[guid] != undefined);

	return guid;
}

async function newFilter() {
	const filters = await getFilters();

	const guid = nextGuid(filters);
	const filter = getDefaultFilter();

	filters[guid] = filter;
	document.getElementById("filters").appendChild(buildFilter(filter, guid));

	browser.storage.local.set({ filters });
}

async function deleteFilter(guid) {
	const filters = await getFilters();
	delete filters[guid];

	const node = document.getElementById(guid);
	document.getElementById("filters").removeChild(node);

	browser.storage.local
		.set({ filters })
		.then(undefined, (e) => console.log(e));
}

function guidToFilter(guid) {
	const regexSource = document.getElementById("regex_source" + guid).value;
	const regexFlags = document.getElementById("regex_flags" + guid).value;
	const target = document.getElementById("target" + guid).value;

	return {
		regex: new RegExp(regexSource, regexFlags),
		target: target,
	};
}

async function updateFilter(guid) {
	const filters = await getFilters();
	filters[guid] = guidToFilter(guid);

	browser.storage.local
		.set({ filters })
		.then(undefined, (e) => console.log(e));
}

window.addEventListener("load", () => {
	document.getElementById("btn-new").addEventListener("click", newFilter);
	refreshFilters();
});
