const browser = require("webextension-polyfill");

let enableSwitch = document.getElementById("enableSwitch");
browser.storage.local
	.get()
	.then((result) => (enableSwitch.checked = result.enabled));

enableSwitch.addEventListener("change", function () {
	browser.storage.local.set({ enabled: this.checked });
});

document
	.getElementById("btn-settings")
	.addEventListener("click", () => browser.runtime.openOptionsPage());
